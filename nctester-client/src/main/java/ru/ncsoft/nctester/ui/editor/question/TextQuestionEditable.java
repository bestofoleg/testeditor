package ru.ncsoft.nctester.ui.editor.question;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;

import ru.ncsoft.nctester.json.model.QuestionEntity;

public class TextQuestionEditable extends QuestionEditable
{
	
	private TextArea rightAnswerTextArea;
	
	private Label rightAnswerInfoLabel;
	
	public TextQuestionEditable()
	{
		
		super();
		
		this.rightAnswerInfoLabel = new Label("Right answer:");
		this.rightAnswerTextArea = new TextArea();
		
		this.rightAnswerTextArea.setHeight("50px");
		this.rightAnswerTextArea.setWidth("250px");
		
		this.add(this.rightAnswerInfoLabel);
		this.add(this.rightAnswerTextArea);
		
	}

	@Override
	public QuestionEntity getQuestionEntity() 
	{
		QuestionEntity result = super.getQuestionEntity();
		
		result.setRightAnswerText(this.rightAnswerTextArea.getText());
		
		return result;
	}
	
	
	
}
