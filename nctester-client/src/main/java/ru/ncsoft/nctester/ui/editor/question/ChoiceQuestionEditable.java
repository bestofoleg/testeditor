package ru.ncsoft.nctester.ui.editor.question;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.VerticalPanel;

import ru.ncsoft.nctester.json.model.ChoiceEntity;
import ru.ncsoft.nctester.json.model.QuestionEntity;
import ru.ncsoft.nctester.json.model.QuestionTypeEntity;


public class ChoiceQuestionEditable extends QuestionEditable
{
	
	private Button addCheckBoxButton;
	
	private VerticalPanel choicesBoxesPanel;
	
	private List <ChoicePanel> choices;
	
	public ChoiceQuestionEditable()
	{
		super();
		
		this.choices = new ArrayList <> ();
		this.choicesBoxesPanel = new VerticalPanel();
		this.addCheckBoxButton = new Button("Add answer");
		
		this.addCheckBoxButton.addClickHandler((h) -> {
			
			ChoicePanel choicePanel = new ChoicePanel(this.choices); 
			
			this.choicesBoxesPanel.add(choicePanel);
			this.choices.add(choicePanel);
			
		});
		
		this.add(this.choicesBoxesPanel);
		this.add(this.addCheckBoxButton);
	}
	
	@Override
	public QuestionEntity getQuestionEntity() 
	{
		
		QuestionEntity result = super.getQuestionEntity();
		
		result.setQuestionType(QuestionTypeEntity.valueOf("CHECK_BOX"));
		
		this.choices.forEach(p -> {
			
			ChoiceEntity c = new ChoiceEntity(p.getChoiceTextBox().getText());
			c.setIsRightAnswer(p.isRightAnswer());
			result.addChoice(c);
			
		});
		
		return result;
		
	}
	
}
