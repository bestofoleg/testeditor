package ru.ncsoft.nctester.ui.creator;

public interface ICreator <T>
{
	
	T generateInstance();
	
}
