package ru.ncsoft.nctester.ui.editor.question;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;

import ru.ncsoft.nctester.json.model.QuestionEntity;
import ru.ncsoft.nctester.json.model.QuestionTypeEntity;

public class QuestionEditable extends AbstractQuestion 
{
	
	protected Label infoToDoLabel;
	
	protected TextArea questionTextArea;
	
	protected Button removeQuestionButton;	
	
	public QuestionEditable()
	{
		
		this.infoToDoLabel = new Label("Question:");
		
		this.questionTextArea = new TextArea();
		questionTextArea.setEnabled(true);
		questionTextArea.setWidth("250px");
		questionTextArea.setHeight("150px");
		
		this.removeQuestionButton = new Button("X");
		this.removeQuestionButton.addClickHandler((h)->{
			QuestionEditable.this.removeFromParent();
		});
		
		this.add(this.removeQuestionButton);
		this.add(infoToDoLabel);
		this.add(questionTextArea);
		
		this.getElement().getStyle().setProperty("border","#577482 groove 4px");
		
	}

	@Override
	public QuestionEntity getQuestionEntity() 
	{
		
		QuestionEntity result = new QuestionEntity();
		
		result.setQuestionType(QuestionTypeEntity.TEXT);
		result.setMessage(this.questionTextArea.getText());
		//result.addChoice();
		
		return result;
		
	}	
	
}
