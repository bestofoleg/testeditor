package ru.ncsoft.nctester.ui;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;

import ru.ncsoft.nctester.json.model.TestEntity;
import ru.ncsoft.nctester.service.TestService;
import ru.ncsoft.nctester.ui.editor.question.QuestionsPanel;
import ru.ncsoft.nctester.ui.editor.question.QuestionsTypeListBox;
import ru.ncsoft.nctester.ui.question.factory.ChoiceQuestionFactory;
import ru.ncsoft.nctester.ui.question.factory.TextQuestionFactory;
import ru.ncsoft.nctester.ui.question.handler.AddEditableQuestionHandler;

public class ToolSetPanel extends HorizontalPanel
{	
	
	private QuestionsTypeListBox questionTypes;
	
	private Button addQuestionButton;
	
	private QuestionsPanel questionsPanel;
	
	private Button saveTestButton;
	
	private TestService testService = GWT.create(TestService.class);
	
	public ToolSetPanel(QuestionsPanel questionsPanel) 
	{
		
		super();
		
		this.saveTestButton = new Button("Save");
		
		this.saveTestButton.addClickHandler((h) -> {
			
			TestEntity testEntity = this.questionsPanel.getTestEntity();
			
			testService.saveTest(testEntity, new MethodCallback <TestEntity> () {

				@Override
				public void onFailure(Method method, Throwable exception) 
				{
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess(Method method, TestEntity response) 
				{
					
					Window.alert("Successful saved!");
					
				}
				
			});
			
		});
		
		this.questionsPanel = questionsPanel;
		
		this.addQuestionButton = new Button("Add question");
		
		this.questionTypes = new QuestionsTypeListBox();
		
		this.questionTypes.addItem("Text question", new TextQuestionFactory());
		this.questionTypes.addItem("Choice question", new ChoiceQuestionFactory());
		
		
		this.addQuestionButton.addClickHandler(
					new AddEditableQuestionHandler(this.questionTypes, this.questionsPanel)
				);
		
		this.add(this.addQuestionButton);
		this.add(this.questionTypes);
		this.add(this.saveTestButton);
		
	}
	
}
