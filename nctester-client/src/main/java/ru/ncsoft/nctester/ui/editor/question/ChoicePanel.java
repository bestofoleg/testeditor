package ru.ncsoft.nctester.ui.editor.question;

import java.util.List;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;

public class ChoicePanel extends HorizontalPanel
{
	
	private TextBox choiceTextBox;
	
	private Button removeButton;

	private CheckBox isRightAnswerCheckBox;
	
	public ChoicePanel(List <ChoicePanel> choicePanels) 
	{
		this.isRightAnswerCheckBox = new CheckBox("It's right answer");
		this.choiceTextBox = new TextBox();
		this.choiceTextBox.setText("Possible answer");
		
		this.removeButton = new Button("X");
		this.removeButton.addClickHandler((h) -> {
			
			this.removeFromParent();
			choicePanels.remove(this);
			
		});
		
		this.add(this.choiceTextBox);
		this.add(this.isRightAnswerCheckBox);
		this.add(this.removeButton);
		
	}

	public TextBox getChoiceTextBox() 
	{
		return choiceTextBox;
	}
	
	public Boolean isRightAnswer()
	{
		return this.isRightAnswerCheckBox.getValue();
	}

	@Override
	public String toString() 
	{
		return "ChoicePanel ["+this.choiceTextBox.getText()+"]";
	}
	
}
