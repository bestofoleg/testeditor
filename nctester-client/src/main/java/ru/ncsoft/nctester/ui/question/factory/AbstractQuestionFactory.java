package ru.ncsoft.nctester.ui.question.factory;

import ru.ncsoft.nctester.ui.editor.question.AbstractQuestion;

public interface AbstractQuestionFactory 
{
	AbstractQuestion createQuestion();
}
