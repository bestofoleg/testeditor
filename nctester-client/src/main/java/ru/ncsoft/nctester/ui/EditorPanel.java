package ru.ncsoft.nctester.ui;

import com.google.gwt.user.client.ui.VerticalPanel;

import ru.ncsoft.nctester.ui.editor.question.QuestionsPanel;

public class EditorPanel extends VerticalPanel
{	
	
	private QuestionsPanel questionsPanel;
	
	private ToolSetPanel toolSetPanel;
	
	public EditorPanel()
	{
		
		super();
		
		this.questionsPanel = new QuestionsPanel();
		this.toolSetPanel = new ToolSetPanel(this.questionsPanel);
		
		this.add(this.questionsPanel);
		this.add(toolSetPanel);
		
	}
	
}
