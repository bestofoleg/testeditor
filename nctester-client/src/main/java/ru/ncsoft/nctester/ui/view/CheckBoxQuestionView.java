package ru.ncsoft.nctester.ui.view;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.ui.CheckBox;

import ru.ncsoft.nctester.json.model.QuestionTypeEntity;
import ru.ncsoft.nctester.json.natives.model.ChoiceEntity;

public class CheckBoxQuestionView extends QuestionView
{
	
	private List <CheckBox> checkBoxes;
	
	private JsArray <ChoiceEntity> choices;
	
	public CheckBoxQuestionView(String taskText, JsArray <ChoiceEntity> choices)
	{
		
		super(taskText);
		
		this.checkBoxes = new ArrayList<>();
		
		this.choices = choices;
		
		this.questionType = QuestionTypeEntity.CHECK_BOX;
		
		final int choicesCount = choices.length();
		
		for(int i = 0;i < choicesCount;i ++)
		{
			
			CheckBox cheackBox = new CheckBox(choices.get(i).getTitle());
			
			this.checkBoxes.add(cheackBox);
			
			this.add(cheackBox);
			
		}
		
	}
	
	@Override
	public List <String> getAnswerTexts()
	{
		
		List <String> result = new ArrayList <> ();
		
		final int choicesCount = this.choices.length();
		
		for(int choiceIndex = 0;choiceIndex < choicesCount;choiceIndex ++)
		{
			if(this.checkBoxes.get(choiceIndex).getValue())
			{
				result.add(choices.get(choiceIndex).getTitle());
			}
		}
		
		return result;
		
	}
	
}
