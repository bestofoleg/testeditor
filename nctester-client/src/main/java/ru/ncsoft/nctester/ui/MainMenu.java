package ru.ncsoft.nctester.ui;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class MainMenu extends VerticalPanel
{
	
	private Button createNewTestButton;
	
	private Button showTestsButton;
	
	private TextBox searchTestByNameTextBox;
	
	private EditorPanel editorPanel;
	
	private HorizontalPanel mainButtonsPanel;
	
	private VerticalPanel mainViewPanel;
	
	public MainMenu()
	{
		
		super();
		
		this.mainViewPanel = new VerticalPanel();
		this.mainButtonsPanel = new HorizontalPanel();
		
		this.createNewTestButton = new Button("New Test");
		
		this.createNewTestButton.addClickHandler((h) -> {
			
			mainViewPanel.clear();
			editorPanel = new EditorPanel();
			mainViewPanel.add(editorPanel);
			
		});
		
		this.showTestsButton = new Button("Show Tests");
		
		this.showTestsButton.addClickHandler((h) -> {
			
			mainViewPanel.clear();
			
			SearchedResultPanel searchResultPanel = new SearchedResultPanel(searchTestByNameTextBox.getText());
			
			mainViewPanel.add(searchResultPanel);
			
		});
		
		this.searchTestByNameTextBox = new TextBox();
		
		this.mainButtonsPanel.add(this.createNewTestButton);
		this.mainButtonsPanel.add(this.showTestsButton);
		this.mainButtonsPanel.add(this.searchTestByNameTextBox);
		this.add(this.mainButtonsPanel);
		this.add(this.mainViewPanel);
		
	}
	
}
