package ru.ncsoft.nctester.ui.question.handler;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;

import ru.ncsoft.nctester.answer.UserAnswersCollector;
import ru.ncsoft.nctester.json.model.AnswersOnQuestions;
import ru.ncsoft.nctester.json.natives.model.Assessment;
import ru.ncsoft.nctester.json.natives.model.JSONString;
import ru.ncsoft.nctester.json.natives.model.TestEntity;
import ru.ncsoft.nctester.service.AnswerService;
import ru.ncsoft.nctester.ui.view.TestView;

public class SendAnswerHandler implements ClickHandler
{

	private AnswerService answerService = GWT.create(AnswerService.class);
	
	private TestEntity testEntity;
	
	private TestView testView;
	
	public SendAnswerHandler(TestEntity testEntity, TestView testView) 
	{
		this.testEntity = testEntity;
		this.testView = testView;
	}

	@Override
	public void onClick(ClickEvent event) 
	{
		
		UserAnswersCollector answersCollector = new UserAnswersCollector(
				Long.parseLong(testEntity.getId()+""), 
				testView.getQuestionViews()
			);
		
		AnswersOnQuestions answers = answersCollector.getAnswers();
		
		answerService.getAssessment(answers, new MethodCallback <JSONString> () {

			@Override
			public void onFailure(Method method, Throwable exception) {
				Window.alert(exception.getMessage());
				
			}

			@Override
			public void onSuccess(Method method, JSONString response) {
				
				Assessment assessment = JsonUtils.safeEval(response.getJstring());
				
				testView.clear();
				testView.add(new Label(assessment.getRightAnswersQuantity()+"/"+assessment.getAnswersQuantity()));
				
			}
			
		});
		
	}
	
}
