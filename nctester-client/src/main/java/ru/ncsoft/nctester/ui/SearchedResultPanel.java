package ru.ncsoft.nctester.ui;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import ru.ncsoft.nctester.json.model.QueryString;
import ru.ncsoft.nctester.json.natives.model.JSONString;
import ru.ncsoft.nctester.json.natives.model.TestEntity;
import ru.ncsoft.nctester.json.natives.model.Tests;
import ru.ncsoft.nctester.service.TestService;
import ru.ncsoft.nctester.ui.creator.TestCreator;

public class SearchedResultPanel extends VerticalPanel
{
	
	private TestService testService = GWT.create(TestService.class);
	
	public SearchedResultPanel(String searchQueryString)
	{
		
		super();
	
		this.executeQuerySearch(searchQueryString);
		
	}
	
	private void executeQuerySearch(String queryString)
	{
		testService.getByQueryString(new QueryString(queryString), new MethodCallback <JSONString> () {

			@Override
			public void onFailure(Method method, Throwable exception) 
			{}

			@Override
			public void onSuccess(Method method, JSONString response) 
			{
				
				Tests tests = JsonUtils.safeEval(response.getJstring());
				
				JsArray <TestEntity> testEntities = tests.getTests();
				
				showEntities(testEntities);
				
			}
			
		});
	}
	
	private void showEntities(JsArray <TestEntity> testEntities)
	{
		if(testEntities.length() != 0)
		{
			for(int i = 0;i < testEntities.length();i ++)
			{
				Button linkBtn = 
						new Button("ID : " + testEntities.get(i).getId() + " Title : " + testEntities.get(i).getTitle());
				
				
				int index = i;
				linkBtn.addClickHandler((h) -> {
					
					clear();
					TestCreator testCreator = new TestCreator(testEntities.get(index));
					add(testCreator.generateInstance());
					
				});
				
				add(linkBtn);
			}
		}
		else
		{
			add(new Label("Not found!"));
		}
		
	}
	
}
