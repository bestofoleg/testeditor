package ru.ncsoft.nctester.ui.editor.question;

import java.util.HashMap;
import java.util.Map;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import ru.ncsoft.nctester.ui.question.factory.AbstractQuestionFactory;

public class QuestionsTypeListBox extends VerticalPanel
{
	
	private Map <String, AbstractQuestionFactory> implsMap;
	
	private ListBox questionsTypes;
	
	public QuestionsTypeListBox() 
	{
		this.implsMap = new HashMap<>();
		this.questionsTypes = new ListBox();
		this.add(this.questionsTypes);
	}
	
	public void addItem(String itemTitle, AbstractQuestionFactory questionFactory)
	{
		this.questionsTypes.addItem(itemTitle);
		this.implsMap.put(itemTitle, questionFactory);
	}
	
	public AbstractQuestion getSelectedItem()
	{
		return this.implsMap.get(this.questionsTypes.getSelectedItemText()).createQuestion();
	}
	
}
