package ru.ncsoft.nctester.ui.question.factory;

import ru.ncsoft.nctester.ui.editor.question.AbstractQuestion;
import ru.ncsoft.nctester.ui.editor.question.ChoiceQuestionEditable;

public class ChoiceQuestionFactory implements AbstractQuestionFactory 
{

	@Override
	public AbstractQuestion createQuestion() 
	{
		return new ChoiceQuestionEditable();
	}

}
