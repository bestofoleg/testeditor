package ru.ncsoft.nctester.ui.question.handler;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

import ru.ncsoft.nctester.ui.editor.question.QuestionEditable;
import ru.ncsoft.nctester.ui.editor.question.QuestionsPanel;
import ru.ncsoft.nctester.ui.editor.question.QuestionsTypeListBox;

public class AddEditableQuestionHandler implements ClickHandler 
{

	private QuestionsTypeListBox questionsTypeListBox;
	
	private QuestionsPanel questionsPanel;
	
	public AddEditableQuestionHandler(QuestionsTypeListBox questionsTypeListBox, QuestionsPanel questionsPanel) 
	{
		super();
		this.questionsTypeListBox = questionsTypeListBox;
		this.questionsPanel = questionsPanel;
	}

	@Override
	public void onClick(ClickEvent event) 
	{
		
		this.questionsPanel.addQuestion((QuestionEditable) this.questionsTypeListBox.getSelectedItem());
		
	}

}
