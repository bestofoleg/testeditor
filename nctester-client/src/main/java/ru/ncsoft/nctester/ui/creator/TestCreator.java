package ru.ncsoft.nctester.ui.creator;


import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.Button;
import ru.ncsoft.nctester.json.natives.model.QuestionEntity;
import ru.ncsoft.nctester.json.natives.model.TestEntity;
import ru.ncsoft.nctester.service.AnswerService;
import ru.ncsoft.nctester.ui.question.handler.SendAnswerHandler;
import ru.ncsoft.nctester.ui.view.CheckBoxQuestionView;
import ru.ncsoft.nctester.ui.view.RadioButtonQuestionView;
import ru.ncsoft.nctester.ui.view.TestView;
import ru.ncsoft.nctester.ui.view.TextQuestionView;

public class TestCreator implements ICreator <TestView>
{

	private TestEntity testEntity;
	
	private Button sendAnswerButton;
	
	public TestCreator(TestEntity testEntity)
	{
		this.testEntity = testEntity;
	}
	
	@Override
	public TestView generateInstance() 
	{
		
		TestView testView = new TestView();
		
		JsArray<QuestionEntity> questions = this.testEntity.getQuestion();
		
		List <QuestionEntity> listForSort = new ArrayList<>();
		
		final int questionsCount = questions.length();
		
		for(int questionIndex = 0;questionIndex < questionsCount;questionIndex ++)
		{
			listForSort.add(questions.get(questionIndex));
		}
		
		for(QuestionEntity questionEntity : listForSort)
		{
			this.determindQuestionType(questionEntity, testView);
		}
		
		this.sendAnswerButton = new Button("Send answers!");
		
		this.sendAnswerButton.addClickHandler(new SendAnswerHandler(this.testEntity, testView));
		
		testView.add(this.sendAnswerButton);
		
		return testView;
	}
	
	private void determindQuestionType(QuestionEntity questionEntity, TestView testView)
	{
		if(questionEntity.getQuestionType().equals("CHECK_BOX"))
		{
			testView.add(new CheckBoxQuestionView(
						
						questionEntity.getMessage(), 
						questionEntity.getChoice()
					
					));
		}
		if(questionEntity.getQuestionType().equals("TEXT"))
		{
			testView.add(new TextQuestionView(questionEntity.getMessage()));
		}
		if(questionEntity.getQuestionType().equals("RADIO_BUTTON"))
		{
			testView.add(new RadioButtonQuestionView(
					
					questionEntity.getMessage(), 
					questionEntity.getChoice(),
					questionEntity.getId()
				
				));
		}
	}
	
}
