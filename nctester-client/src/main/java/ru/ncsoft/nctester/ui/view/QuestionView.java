package ru.ncsoft.nctester.ui.view;

import java.util.List;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import ru.ncsoft.nctester.json.model.QuestionTypeEntity;

public abstract class QuestionView extends VerticalPanel
{
	
	private Label taskTextLabel;
	
	protected QuestionTypeEntity questionType;
	
	public QuestionView(String taskText)
	{
		
		super();
		
		this.questionType = null;
		
		this.getElement().getStyle().setProperty("border","#577482 groove 4px");
		
		this.taskTextLabel = new Label(taskText);
		
		this.add(this.taskTextLabel);
		
	}
	
	public abstract List <String> getAnswerTexts();

	public QuestionTypeEntity getQuestionType() {
		return questionType;
	}

	public void setQuestionType(QuestionTypeEntity questionType) {
		this.questionType = questionType;
	}
	
}
