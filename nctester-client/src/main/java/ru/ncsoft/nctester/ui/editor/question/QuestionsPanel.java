package ru.ncsoft.nctester.ui.editor.question;

import java.util.ArrayList;
import java.util.List;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import ru.ncsoft.nctester.json.model.TestEntity;

public class QuestionsPanel extends VerticalPanel 
{	
	
	private TextBox testNameTextBox;
	
	private Label testNameLabel;
	
	private List <QuestionEditable> questions;
	
	public QuestionsPanel()
	{
		super();
		this.questions = new ArrayList <> ();
		this.testNameTextBox = new TextBox();
		this.testNameTextBox.setText("unnamed");
		
		this.testNameLabel = new Label("Test name:");
		
		this.add(this.testNameLabel);
		this.add(this.testNameTextBox);
	}
	
	public TestEntity getTestEntity()
	{
		TestEntity result = new TestEntity(this.testNameTextBox.getText());
		
		this.questions.forEach(p -> result.addQuestion(p.getQuestionEntity()));
		
		return result;
	}
	
	public void addQuestion(QuestionEditable question)
	{
		this.add(question);
		this.questions.add(question);		
	}
	
}
