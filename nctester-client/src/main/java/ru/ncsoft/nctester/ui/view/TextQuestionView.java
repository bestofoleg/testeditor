package ru.ncsoft.nctester.ui.view;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

import ru.ncsoft.nctester.json.model.QuestionTypeEntity;

public class TextQuestionView extends QuestionView
{
	
	private TextBox answerTextBox;

	public TextQuestionView(String taskText)
	{
		
		super(taskText);
		
		this.questionType = QuestionTypeEntity.TEXT;
		
		this.answerTextBox = new TextBox();
		
		this.add(new Label("Your answer:"));
		this.add(this.answerTextBox);
		
	}

	@Override
	public List<String> getAnswerTexts() 
	{
		List <String> answers = new ArrayList <> ();
		
		answers.add(this.answerTextBox.getText());
		
		return answers;
	}
	
}
