package ru.ncsoft.nctester.ui.view;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.RadioButton;

import ru.ncsoft.nctester.json.model.QuestionTypeEntity;
import ru.ncsoft.nctester.json.natives.model.ChoiceEntity;

public class RadioButtonQuestionView extends QuestionView 
{
	private List <RadioButton> radioButtons;
	
	private JsArray <ChoiceEntity> choices;
	
	public RadioButtonQuestionView(String taskText, JsArray <ChoiceEntity> choices, Long questionNumber)
	{
		
		super(taskText);
		
		this.radioButtons = new ArrayList <> ();
		
		this.choices = choices;
		
		this.questionType = QuestionTypeEntity.RADIO_BUTTON;
		
		final int choicesCount = choices.length();
		
		for(int i = 0;i < choicesCount;i ++)
		{
			
			RadioButton radioButton = new RadioButton("choices"+questionNumber, choices.get(i).getTitle());
			
			this.radioButtons.add(radioButton);
			
			this.add(radioButton);
			
		}
		
	}
	
	@Override
	public List <String> getAnswerTexts()
	{
		
		List <String> result = new ArrayList <> ();
		
		final int choicesCount = this.choices.length();
		
		for(int choiceIndex = 0;choiceIndex < choicesCount;choiceIndex ++)
		{
			if(this.radioButtons.get(choiceIndex).getValue())
			{
				result.add(choices.get(choiceIndex).getTitle());
			}
		}
		
		return result;
		
	}
	
}
