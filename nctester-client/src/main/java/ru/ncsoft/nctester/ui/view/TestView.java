package ru.ncsoft.nctester.ui.view;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class TestView extends VerticalPanel
{
	
	private List <QuestionView> questionViews;
	
	public TestView()
	{
		
		super();
		this.questionViews = new ArrayList<>();
		
	}
	
	public void add(QuestionView w)
	{
		
		super.add(w);
		this.questionViews.add(w);
		
	}

	public List<QuestionView> getQuestionViews() 
	{
		return questionViews;
	}
	
}
