package ru.ncsoft.nctester.ui.question.factory;

import ru.ncsoft.nctester.ui.editor.question.AbstractQuestion;
import ru.ncsoft.nctester.ui.editor.question.TextQuestionEditable;

public class TextQuestionFactory implements AbstractQuestionFactory
{

	@Override
	public AbstractQuestion createQuestion()
	{
		return new TextQuestionEditable();
	}
}
