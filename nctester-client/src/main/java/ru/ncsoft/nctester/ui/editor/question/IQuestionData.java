package ru.ncsoft.nctester.ui.editor.question;

import ru.ncsoft.nctester.json.model.QuestionEntity;

public interface IQuestionData 
{
	public QuestionEntity getQuestionEntity();
}
