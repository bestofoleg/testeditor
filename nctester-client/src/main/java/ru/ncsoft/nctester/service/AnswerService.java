package ru.ncsoft.nctester.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.RestService;

import ru.ncsoft.nctester.json.model.AnswersOnQuestions;
import ru.ncsoft.nctester.json.model.Numbers;
import ru.ncsoft.nctester.json.natives.model.JSONString;

@Path("/rest/answer-controller")
public interface AnswerService extends RestService
{
	
	@POST
	@Path("/get-assessment")
	public void getAssessment(AnswersOnQuestions answers, MethodCallback<JSONString> callback);
	
	@POST
	@Path("/get-numbers")
	public void getNumbers(Numbers answers, MethodCallback<JSONString> callback);
	
}
