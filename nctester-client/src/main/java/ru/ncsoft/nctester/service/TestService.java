package ru.ncsoft.nctester.service;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.RestService;

import ru.ncsoft.nctester.json.model.QueryString;
import ru.ncsoft.nctester.json.model.TestEntity;
import ru.ncsoft.nctester.json.natives.model.JSONString;

@Path("/rest/test-controller")
public interface TestService extends RestService
{
	
	@POST
	@Path("/save-test")
	public void saveTest(TestEntity test, MethodCallback <TestEntity> callback);

	@GET
	@Path("/get-all")
	public void getAll(MethodCallback <JSONString> callback);
	
	@POST
	@Path("/get-by-query-string")
	public void getByQueryString(QueryString queryString, MethodCallback <JSONString> callback);
	
	@GET
	@Path("/get-debug")
	public void getDebug(MethodCallback <JSONString> callback);
	
}
