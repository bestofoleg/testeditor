package ru.ncsoft.nctester.json.natives.model;

import java.io.Serializable;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

@SuppressWarnings("serial")
public class Debug extends JavaScriptObject implements Serializable
{
	
	protected Debug()
	{}

	public final native JsArray getNumbers() 
	/*-{
		return this.numbers;
	}-*/;
	
}