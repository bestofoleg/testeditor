package ru.ncsoft.nctester.json.natives.model;

import java.io.Serializable;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

@SuppressWarnings("serial")
public class Tests extends JavaScriptObject implements Serializable
{

	protected Tests()
	{}
	
	public final native JsArray <TestEntity> getTests() 
	/*-{
		return this.tests;
	}-*/;
	
}
