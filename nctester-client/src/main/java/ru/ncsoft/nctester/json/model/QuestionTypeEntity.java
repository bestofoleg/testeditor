package ru.ncsoft.nctester.json.model;

public enum QuestionTypeEntity
{
	TEXT, RADIO_BUTTON, CHECK_BOX
}
