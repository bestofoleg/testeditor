package ru.ncsoft.nctester.json.natives.model;

import java.io.Serializable;

import com.google.gwt.core.client.JavaScriptObject;

@SuppressWarnings("serial")
public class Assessment extends JavaScriptObject implements Serializable
{
	
	protected Assessment() 
	{}

	public final native Integer getRightAnswersQuantity() 
	/*-{
		return this.rightAnswersQuantity;
	}-*/;

	public final native Integer getAnswersQuantity() 
	/*-{
		return this.answersQuantity;
	}-*/;
	
}
