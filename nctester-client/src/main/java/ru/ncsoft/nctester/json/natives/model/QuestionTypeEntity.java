package ru.ncsoft.nctester.json.natives.model;

public enum QuestionTypeEntity
{
	TEXT, RADIO_BUTTON, CHECK_BOX
}
