package ru.ncsoft.nctester.json.natives.model;

import java.io.Serializable;

import com.google.gwt.core.client.JavaScriptObject;

@SuppressWarnings("serial")
public class ChoiceEntity extends JavaScriptObject implements Serializable
{
	
	protected ChoiceEntity()
	{}

	public final native String getTitle() 
	/*-{
		return this.title;
	}-*/;

	public final native Long getId() 
	/*-{
		return this.id;
	}-*/;
	
	public final native Boolean getIsRightAnswer() 
	/*-{
		return this.isRightAnswer;
	}-*/;
	
}
