package ru.ncsoft.nctester.json.natives.model;

import java.io.Serializable;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

@SuppressWarnings("serial")
public class TestEntity extends JavaScriptObject implements Serializable
{
	
	protected TestEntity()
	{}

	public final native Long getId() 
	/*-{
		return this.id;
	}-*/;

	public final native String getTitle() 
	/*-{
		return this.title;
	}-*/;
	
	public final native JsArray <QuestionEntity> getQuestion() 
	/*-{
		return this.question;
	}-*/;
	
}
