package ru.ncsoft.nctester.json.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("serial")
public class Tests implements Serializable
{
	
	private List <TestEntity> tests;
	
	public Tests()
	{
		this.tests = new ArrayList<>();
	}
	
	public Tests(List <TestEntity> tests)
	{
		this.tests = tests;
	}

	public List <TestEntity> getTests() 
	{
		return this.tests;
	}

	public void setTests(List<TestEntity> tests) 
	{
		this.tests = tests;
	}
	
	public void addTest(TestEntity test) 
	{
		this.tests.add(test);
	}

	@Override
	public String toString() 
	{	
		return "Tests [tests=" + Arrays.toString(tests.toArray()) + "]";
	}
	
}
