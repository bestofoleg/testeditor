package ru.ncsoft.nctester.json.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class TestEntity implements Serializable
{
	
	private Long id;
	
	private String title;
	
	private List <QuestionEntity> question;
	
	public TestEntity()
	{
		this.question = new ArrayList <> ();
	}
	
	public TestEntity(String title)
	{
		this.title = title;
		this.question = new ArrayList <> ();
	}

	public Long getId() 
	{
		return this.id;
	}

	public String getTitle() 
	{
		return this.title;
	}

	public void setTitle(String title) 
	{
		this.title = title;
	}

	public void addQuestion(QuestionEntity questionEntity)
	{
		this.question.add(questionEntity);
	}
	
	public List <QuestionEntity> getQuestion() 
	{
		return this.question;
	}

	@Override
	public String toString() 
	{
		return "TestEntity [id=" + id + ", title=" + title + ", question=" + question + "]";
	}	
	
}
