package ru.ncsoft.nctester.json.natives.model;

import java.io.Serializable;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

@SuppressWarnings("serial")
public class QuestionEntity extends JavaScriptObject implements Serializable
{
	
	protected QuestionEntity()
	{}

	public final native String getMessage() 
	/*-{
		return this.message;
	}-*/;
	
	public final native String getRightAnswerText() 
	/*-{
		return this.rightAnswerText;
	}-*/;

	public final native String getQuestionType() 
	/*-{
		return this.questionType;
	}-*/;

	public final native Long getId() 
	/*-{
		return this.id;
	}-*/;

	public final native JsArray<ChoiceEntity> getChoice() 
	/*-{
		return this.choice;
	}-*/;
		
}