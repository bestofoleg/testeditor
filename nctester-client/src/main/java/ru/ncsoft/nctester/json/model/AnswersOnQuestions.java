package ru.ncsoft.nctester.json.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("serial")
public class AnswersOnQuestions implements Serializable
{
	
	private long testId;
	
	private List <AnswerOnQuestion> answersList;
	
	public AnswersOnQuestions()
	{
		this.answersList = new ArrayList <> ();
	}
	
	public AnswersOnQuestions(List <AnswerOnQuestion> answersList)
	{
		this.answersList = answersList;
	}
	
	public long getTestId() 
	{
		return testId;
	}

	public void setTestId(long testId) 
	{
		this.testId = testId;
	}

	public List<AnswerOnQuestion> getAnswersList() 
	{
		return answersList;
	}

	public void setAnswersList(List<AnswerOnQuestion> answersList) 
	{
		this.answersList = answersList;
	}

	@Override
	public String toString() 
	{
		return "Answers [answersList=" + Arrays.toString(this.answersList.toArray()) + "]";
	}
	
}
