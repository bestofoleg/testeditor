package ru.ncsoft.nctester.json.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class QueryString implements Serializable
{
	
	private String queryString;
	
	public QueryString()
	{
		this.queryString = "";
	}
	
	public QueryString(String str)
	{
		this.queryString = str;
	}
	
	public String getQueryString()
	{
		return this.queryString;
	}
	
	public void setQueryString(String str)
	{
		this.queryString = str;
	}

	@Override
	public String toString() 
	{
		return "QueryString [queryString=" + queryString + "]";
	}
	
}
