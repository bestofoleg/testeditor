package ru.ncsoft.nctester.json.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ChoiceEntity implements Serializable
{
	
	private Long id;
	
	private String title;
	
	private Boolean isRightAnswer;

	public ChoiceEntity() 
	{}

	public ChoiceEntity(String title) 
	{
		this.title = title;
	}

	public String getTitle() 
	{
		return this.title;
	}

	public void setTitle(String title) 
	{
		this.title = title;
	}

	public Long getId() 
	{
		return this.id;
	}

	public Boolean getIsRightAnswer() 
	{
		return isRightAnswer;
	}

	public void setIsRightAnswer(Boolean isRightAnswer) 
	{
		this.isRightAnswer = isRightAnswer;
	}

	@Override
	public String toString() 
	{
		return "ChoiceEntity [id=" + id + ", title=" + title + "]";
	}	
	
}
