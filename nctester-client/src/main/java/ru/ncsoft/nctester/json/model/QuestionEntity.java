package ru.ncsoft.nctester.json.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("serial")
public class QuestionEntity implements Serializable
{

	private Long id;

	private String message;
	
	private String rightAnswerText;
	
	private List <ChoiceEntity> choice;
	
	private QuestionTypeEntity questionType;

	public QuestionEntity() 
	{
		this.choice = new ArrayList <> ();
	}

	public QuestionEntity(String message, QuestionTypeEntity questionType) 
	{
		this.message = message;
		this.questionType = questionType;
	}
	

	public String getMessage() 
	{
		return this.message;
	}

	public void setMessage(String message) 
	{
		this.message = message;
	}

	public String getRightAnswerText() 
	{
		return rightAnswerText;
	}

	public void setRightAnswerText(String rightAnswerText) 
	{
		this.rightAnswerText = rightAnswerText;
	}

	public QuestionTypeEntity getQuestionType() 
	{
		return this.questionType;
	}

	public void setQuestionType(QuestionTypeEntity questionType) 
	{
		this.questionType = questionType;
	}

	public Long getId() 
	{
		return this.id;
	}

	public List<ChoiceEntity> getChoice() 
	{
		return this.choice;
	}
	
	public void addChoice(ChoiceEntity choiceEntity)
	{
		this.choice.add(choiceEntity);
	}

	@Override
	public String toString() 
	{
		return "QuestionEntity [id=" + id + ", message=" + message + ", choice=" + choice + ", questionType="
				+ questionType + "]";
	}
	
}
