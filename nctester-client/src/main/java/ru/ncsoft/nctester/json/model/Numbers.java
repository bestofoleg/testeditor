package ru.ncsoft.nctester.json.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Numbers implements Serializable
{
	
	private long number;
	
	public Numbers()
	{}

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}
	
}
