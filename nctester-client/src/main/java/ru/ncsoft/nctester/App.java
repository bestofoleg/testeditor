package ru.ncsoft.nctester;

import org.fusesource.restygwt.client.Defaults;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;

import ru.ncsoft.nctester.json.model.Numbers;
import ru.ncsoft.nctester.json.natives.model.JSONString;
import ru.ncsoft.nctester.service.AnswerService;
import ru.ncsoft.nctester.ui.MainMenu;


public class App implements EntryPoint 
{
	
	public void onModuleLoad() 
	{
		
		Defaults.setServiceRoot(GWT.getHostPageBaseURL());
		
		final MainMenu mainMenu = new MainMenu();
		
		RootPanel.get("mainContainer").add(mainMenu);		
		
	}
	
}
