package ru.ncsoft.nctester.answer;

import java.util.ArrayList;
import java.util.List;

import ru.ncsoft.nctester.json.model.AnswerOnQuestion;
import ru.ncsoft.nctester.json.model.AnswersOnQuestions;
import ru.ncsoft.nctester.ui.view.QuestionView;

public class UserAnswersCollector 
{
	
	private long testId;
	
	private List <QuestionView> questionViews;
	
	public UserAnswersCollector(long testId, List <QuestionView> questionViews)
	{
		this.testId = testId;
		this.questionViews = questionViews;
	}
	
	public AnswersOnQuestions getAnswers()
	{
		List <AnswerOnQuestion> answersList = new ArrayList <> ();
		
		this.questionViews.forEach(q -> {
			
			List <String> answersString = q.getAnswerTexts();
			AnswerOnQuestion answer = new AnswerOnQuestion(answersString, q.getQuestionType());
			answersList.add(answer);
			
		});
		
		AnswersOnQuestions result = new AnswersOnQuestions(answersList);
		
		result.setTestId(this.testId);
		
		return result;
		
	}
	
}
