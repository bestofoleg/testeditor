package ru.ncsoft.nctester.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ru.ncsoft.nctester.json.model.AnswersOnQuestions;
import ru.ncsoft.nctester.json.model.JSONString;
import ru.ncsoft.nctester.json.model.Numbers;
import ru.ncsoft.nctester.logic.JsonConvertor;
import ru.ncsoft.nctester.service.AssessmentService;

@RestController
@RequestMapping("/answer-controller")
public class AnswerController 
{
	
	@Autowired
	private AssessmentService assessmentService;
	
	public AnswerController()
	{}
	
	@RequestMapping(method = RequestMethod.POST, value = "/get-assessment", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public JSONString getAssessment(@RequestBody AnswersOnQuestions answers)
	{
		System.out.println("Answer controller!");
		return this.assessmentService.makeAssessment(answers);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/get-numbers", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public JSONString getNumbers(@RequestBody Numbers nums)
	{
		
		nums.setNumber(nums.getNumber() + 1);
		return JsonConvertor.entityToJSONString(nums);
	}
	
	
}
