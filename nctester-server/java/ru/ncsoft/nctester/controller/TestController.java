package ru.ncsoft.nctester.controller;

import java.io.IOException;
import java.io.StringWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ru.ncsoft.nctester.entity.TestEntity;
import ru.ncsoft.nctester.json.model.Debug;
import ru.ncsoft.nctester.json.model.JSONString;
import ru.ncsoft.nctester.json.model.QueryString;
import ru.ncsoft.nctester.service.IService;

@RestController
@RequestMapping("/test-controller")
public class TestController
{
	
	@Autowired
    private IService <TestEntity> testService;
	
	public TestController()
	{}
	
	@RequestMapping(method = RequestMethod.POST, value = "/save-test", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public TestEntity saveTest(@RequestBody TestEntity testEntity)
	{
		
		this.testService.save(testEntity);
		
		return testEntity;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/get-by-query-string", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public JSONString getByQueryString(@RequestBody QueryString queryString)
	{
		return this.testService.getByQueryString(queryString.getQueryString());
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/get-all")
	@ResponseBody
	public JSONString getAll()
	{
		return this.testService.getAll();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/get-debug")
	@ResponseBody
	public JSONString getDebug()
	{
		
		Debug d = new Debug();
		
		d.addNumber(1);
		d.addNumber(2);
		d.addNumber(3);
		
		ObjectMapper mapper = new ObjectMapper();
		StringWriter sw = new StringWriter();
		
		try {
			mapper.writeValue(sw, d);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JSONString jstring = new JSONString(sw.toString());
		
		
		return jstring;
	}

}
