package ru.ncsoft.nctester.service;

import ru.ncsoft.nctester.json.model.JSONString;

public interface IService <T>
{
	
	void save(T t);
	
	JSONString getAll();
	
	JSONString getByQueryString(String str);
	
}
