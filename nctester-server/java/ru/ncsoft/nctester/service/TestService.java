package ru.ncsoft.nctester.service;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gwt.core.shared.GWT;

import ru.ncsoft.nctester.dao.IDAO;
import ru.ncsoft.nctester.entity.ChoiceEntity;
import ru.ncsoft.nctester.entity.QuestionEntity;
import ru.ncsoft.nctester.entity.QuestionTypeEntity;
import ru.ncsoft.nctester.entity.TestEntity;
import ru.ncsoft.nctester.json.model.JSONString;
import ru.ncsoft.nctester.json.model.Tests;
import ru.ncsoft.nctester.logic.JsonConvertor;

@Service
public class TestService implements IService <TestEntity>
{
	
	@Autowired
	private IDAO <TestEntity> testDAO;
	
	public TestService()
	{}
	
	@Override
	@Transactional
	public void save(TestEntity testEntity)
	{
		
		List<QuestionEntity> questions = testEntity.getQuestion();
		
		questions.forEach(p -> {p.setQuestionType(getTypeForQuestion(p));});
		
		this.testDAO.save(testEntity);
		
	}
	
	private QuestionTypeEntity getTypeForQuestion(QuestionEntity questionEntity)
	{
		if(questionEntity.getQuestionType().equals(QuestionTypeEntity.TEXT))
		{
			return QuestionTypeEntity.TEXT;
		}
		
		int choicesCount = 0;
		for(ChoiceEntity choice : questionEntity.getChoice())
		{
			if(choice.getIsRightAnswer())
			{
				choicesCount ++;
			}
				
			if(choicesCount > 1)
			{
				return QuestionTypeEntity.CHECK_BOX;
			}
		}
			
		return QuestionTypeEntity.RADIO_BUTTON;
	}
	
	@Override
	@Transactional
	public JSONString getAll()
	{
		
		Tests tests = new Tests(this.testDAO.getAll());
		return JsonConvertor.entityToJSONString(tests);
		
	}

	@Override
	@Transactional
	public JSONString getByQueryString(String str) 
	{
		if(str.equals("") || str == null)
		{
			return getAll();
		}
		
		Tests tests = new Tests(this.testDAO.getByQueryString(str));
		
		return JsonConvertor.entityToJSONString(tests);
	}
	

	
}
