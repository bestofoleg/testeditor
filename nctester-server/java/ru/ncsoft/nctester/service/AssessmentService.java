package ru.ncsoft.nctester.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.ncsoft.nctester.dao.IDAO;
import ru.ncsoft.nctester.entity.TestEntity;
import ru.ncsoft.nctester.json.model.AnswersOnQuestions;
import ru.ncsoft.nctester.json.model.Assessment;
import ru.ncsoft.nctester.json.model.JSONString;
import ru.ncsoft.nctester.logic.AssessmentSystem;
import ru.ncsoft.nctester.logic.JsonConvertor;

@Service
public class AssessmentService 
{
	@Autowired
	private IDAO <TestEntity> testEntityDAO;
	
	@Transactional
	public JSONString makeAssessment(AnswersOnQuestions answers)
	{				
		return JsonConvertor.entityToJSONString(
					AssessmentSystem.getAssessment(answers, this.testEntityDAO.getById(answers.getTestId()))
				);
	}
	
}
