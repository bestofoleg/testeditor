package ru.ncsoft.nctester.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Choice")
public class ChoiceEntity 
{
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "title")
	private String title;

	@Column(name = "isRightAnswer")
	private Boolean isRightAnswer;
	
	public ChoiceEntity() 
	{}

	public ChoiceEntity(String title) 
	{
		this.title = title;
	}

	public String getTitle() 
	{
		return title;
	}

	public void setTitle(String title) 
	{
		this.title = title;
	}

	public Long getId() 
	{
		return id;
	}
	
	public Boolean getIsRightAnswer() 
	{
		return isRightAnswer;
	}

	public void setIsRightAnswer(Boolean isRightAnswer) 
	{
		this.isRightAnswer = isRightAnswer;
	}

	@Override
	public String toString() 
	{
		return "ChoiceEntity [id=" + id + ", title=" + title + "]";
	}		
	
}

