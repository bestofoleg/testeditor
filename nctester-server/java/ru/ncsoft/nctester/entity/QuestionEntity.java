package ru.ncsoft.nctester.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Question")
public class QuestionEntity 
{
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "message")
	private String message;
	
	@Column(name = "rightAnswerText")
	private String rightAnswerText;
	
	@OneToMany(cascade = CascadeType.ALL)
	@Column(name = "choice")
	private List <ChoiceEntity> choice;
	
	@Column(name = "questionType")
	@Enumerated(EnumType.STRING)
	private QuestionTypeEntity questionType;

	public QuestionEntity() 
	{
		this.choice = new ArrayList<>();
	}

	public QuestionEntity(String message, QuestionTypeEntity questionType) 
	{
		this.message = message;
		this.questionType = questionType;
	}
	
	

	public String getMessage() 
	{
		return message;
	}

	public void setMessage(String message) 
	{
		this.message = message;
	}

	public String getRightAnswerText() 
	{
		return rightAnswerText;
	}

	public void setRightAnswerText(String rightAnswerText) 
	{
		this.rightAnswerText = rightAnswerText;
	}

	public QuestionTypeEntity getQuestionType() 
	{
		return questionType;
	}

	public void setQuestionType(QuestionTypeEntity questionType) 
	{
		this.questionType = questionType;
	}

	public Long getId() 
	{
		return id;
	}

	public List<ChoiceEntity> getChoice() 
	{
		return choice;
	}
	
	public void addChoice(ChoiceEntity choiceEntity)
	{
		this.choice.add(choiceEntity);
	}

	@Override
	public String toString() 
	{
		return "QuestionEntity [id=" + id + ", message=" + message + ", choice=" + choice + ", questionType="
				+ questionType + "]";
	}
	
}
