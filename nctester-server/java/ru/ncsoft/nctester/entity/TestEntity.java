package ru.ncsoft.nctester.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Test")
public class TestEntity
{
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "title")
	private String title;
	
	@OneToMany(cascade = CascadeType.ALL)
	@Column(name = "question")
	private List <QuestionEntity> question;
	
	public TestEntity()
	{
		this.question = new ArrayList<>();
	}
	
	public TestEntity(String title)
	{
		this.title = title;
		this.question = new ArrayList<>();
	}

	public Long getId() 
	{
		return id;
	}

	public String getTitle() 
	{
		return title;
	}

	public void setTitle(String title) 
	{
		this.title = title;
	}

	public void addQuestion(QuestionEntity questionEntity)
	{
		this.question.add(questionEntity);
	}
	
	public List<QuestionEntity> getQuestion() 
	{
		return question;
	}

	@Override
	public String toString() 
	{
		return "TestEntity [id=" + id + ", title=" + title + ", question=" + question + "]";
	}	
	
}
