package ru.ncsoft.nctester.json.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QuestionModel implements Serializable
{
	
	private Long id;
	
	private QuestionTypesModel type;
	
	private String text;
	
	private List <ChoiceModel> choices;
	
	public QuestionModel()
	{
		this.choices = new ArrayList <> ();
	}

	public QuestionModel(QuestionTypesModel type, String text, List<ChoiceModel> choice) 
	{
		this.type = type;
		this.text = text;
		this.choices = choice;
	}

	public Long getId() 
	{
		return id;
	}

	public QuestionTypesModel getType() 
	{
		return type;
	}

	public void setType(QuestionTypesModel type) 
	{
		this.type = type;
	}

	public String getText() 
	{
		return text;
	}

	public void setText(String text) 
	{
		this.text = text;
	}

	public List<ChoiceModel> getChoice() 
	{
		return choices;
	}

	public void setChoice(List<ChoiceModel> choice) 
	{
		this.choices = choice;
	}

	@Override
	public String toString() 
	{
		StringBuilder chs = new StringBuilder();
		
		if(this.choices != null && !this.choices.isEmpty())
			{this.choices.forEach(p -> chs.append(p.toString()));}
		else
			{chs.append("[empty]");}
		
		return "QuestionEntity [id=" + id + ", type=" + type.toString() 
					+ ", text=" + text + ", choice=" + chs.toString() + "]";
	}
	
}
