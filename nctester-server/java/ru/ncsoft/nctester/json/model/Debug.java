package ru.ncsoft.nctester.json.model;

import java.util.ArrayList;
import java.util.List;

public class Debug 
{
	
	private List <Integer> numbers;
	
	public Debug()
	{
		this.numbers = new ArrayList <> ();
	}
	
	public void addNumber(Integer number)
	{
		this.numbers.add(number);
	}

	public List <Integer> getNumbers() 
	{
		return numbers;
	}
	
}
