package ru.ncsoft.nctester.json.model;

public class JSONString 
{
	
	private String jstring;
	
	public JSONString()
	{
		this.jstring = "{ }";
	}
	
	public JSONString(String jstring)
	{
		this.jstring = jstring;
	}

	public String getJstring() 
	{
		return jstring;
	}

	public void setJstring(String jstring) 
	{
		this.jstring = jstring;
	}

	@Override
	public String toString() 
	{
		return "JSONString [jstring=" + jstring + "]";
	}
	
}
