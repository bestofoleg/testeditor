package ru.ncsoft.nctester.json.model;

public class QueryString 
{
	
	private String queryString;
	
	public QueryString()
	{
		this.queryString = "";
	}
	
	public QueryString(String str)
	{
		this.queryString = str;
	}
	
	public String getQueryString()
	{
		return this.queryString;
	}
	
	public void setQueryString(String str)
	{
		this.queryString = str;
	}

	@Override
	public String toString() 
	{
		return "QueryString [queryString=" + queryString + "]";
	}
	
}
