package ru.ncsoft.nctester.json.model;

import java.util.ArrayList;
import java.util.List;

public class QuestionListModel 
{
	
	private List <QuestionModel> questionsList;
	
	public QuestionListModel()
	{
		this.questionsList = new ArrayList<>();
	}
	
	public QuestionListModel(List <QuestionModel> questionsList)
	{
		this.questionsList = questionsList;
	}

	public List<QuestionModel> getQuestionsList() 
	{
		return questionsList;
	}

	public void setQuestionsList(List<QuestionModel> questionsList) 
	{
		this.questionsList = questionsList;
	}
	
}
