package ru.ncsoft.nctester.json.model;

import java.io.Serializable;

public class QuestionTypesModel implements Serializable
{
	
	private Long id;
	
	private String typeName;
	
	public QuestionTypesModel()
	{}

	public QuestionTypesModel(String typeName) 
	{
		this.typeName = typeName;
	}

	public Long getId() 
	{
		return id;
	}

	public String getTypeName() 
	{
		return typeName;
	}

	public void setTypeName(String typeName) 
	{
		this.typeName = typeName;
	}
	
}
