package ru.ncsoft.nctester.json.model;

import java.io.Serializable;

public class ChoiceModel implements Serializable
{
	
	private Long id;
	
	private String choiceName;
	
	public ChoiceModel()
	{}

	public ChoiceModel(String choiceName) 
	{
		this.choiceName = choiceName;
	}

	public Long getId() 
	{
		return id;
	}

	public String getChoiceName() 
	{
		return choiceName;
	}

	public void setChoiceName(String choiceName) 
	{
		this.choiceName = choiceName;
	}

	@Override
	public String toString() {
		return "ChoiceEntity [id=" + id + ", choiceName=" + choiceName + "]";
	}
	
}
