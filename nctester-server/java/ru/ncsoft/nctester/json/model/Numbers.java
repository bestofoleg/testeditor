package ru.ncsoft.nctester.json.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Numbers implements Serializable
{
	
	private Long number;
	
	public Numbers()
	{}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}
	
}
