package ru.ncsoft.nctester.json.model;

public class Assessment 
{
	
	private Integer rightAnswersQuantity;
	
	private Integer answersQuantity;

	public Assessment() 
	{}

	public Assessment(Integer rightAnswersQuantity, Integer answersQuantity) 
	{
		this.rightAnswersQuantity = rightAnswersQuantity;
		this.answersQuantity = answersQuantity;
	}

	public Integer getRightAnswersQuantity() 
	{
		return rightAnswersQuantity;
	}

	public void setRightAnswersQuantity(Integer rightAnswersQuantity) 
	{
		this.rightAnswersQuantity = rightAnswersQuantity;
	}

	public Integer getAnswersQuantity() 
	{
		return answersQuantity;
	}

	public void setAnswersQuantity(Integer answersQuantity) 
	{
		this.answersQuantity = answersQuantity;
	}

	@Override
	public String toString() 
	{
		return "Assessment = " + rightAnswersQuantity + "/" + answersQuantity;
	}
	
}
