package ru.ncsoft.nctester.json.model;

import java.util.ArrayList;
import java.util.List;

public class ChoiceListModel 
{
	
	private List <ChoiceModel> choices;
	
	public ChoiceListModel()
	{
		this.choices = new ArrayList <> ();
	}
	
	public ChoiceListModel(List <ChoiceModel> choices)
	{
		this.choices = choices;
	}

	public List<ChoiceModel> getChoices() 
	{
		return choices;
	}

	public void setChoices(List<ChoiceModel> choices) 
	{
		this.choices = choices;
	}
	
}
