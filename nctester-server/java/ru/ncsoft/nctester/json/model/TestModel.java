package ru.ncsoft.nctester.json.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TestModel implements Serializable
{
	private Long id;
	
	private String testName;
	
	private List<QuestionModel> questions;
	
	public TestModel()
	{
		this.testName = "unnamed";
		this.questions = new ArrayList<>();
	}
	
	public TestModel(String testName) 
	{
		this.testName = testName;
		this.questions = new ArrayList<>();
	}

	public TestModel(String testName, List<QuestionModel> questions) 
	{
		this.testName = testName;
		this.questions = questions;
	}

	public String getTestName() 
	{
		return testName;
	}

	public void setTestName(String testName) 
	{
		this.testName = testName;
	}

	public List<QuestionModel> getQuestions() 
	{
		return questions;
	}
	
	public void addQuestion(QuestionModel question)
	{
		this.questions.add(question);
	}
	
	public void addAllQuestions(List <QuestionModel> questions)
	{
		this.questions.addAll(questions);
	}
	
	public void clearQuestionList()
	{
		this.questions.clear();
	}
	
	@Override
	public String toString() 
	{
		StringBuilder qsStr = new StringBuilder();
		
		if(this.questions != null && !this.questions.isEmpty())
			{this.questions.forEach(p -> qsStr.append(p.toString()));}
		else
			{qsStr.append("[empty]");}
		
		return "TestEntity [testName=" + testName + ", questions=" + qsStr.toString() + "]";
	}
	
}
