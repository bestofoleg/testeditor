package ru.ncsoft.nctester.json.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.ncsoft.nctester.entity.TestEntity;

public class Tests 
{
	
	private List <TestEntity> tests;
	
	public Tests()
	{
		this.tests = new ArrayList<>();
	}
	
	public Tests(List <TestEntity> tests)
	{
		this.tests = tests;
	}

	public List<TestEntity> getTests() 
	{
		return tests;
	}

	public void setTests(List<TestEntity> tests) 
	{
		this.tests = tests;
	}
	
	public void addTest(TestEntity test) 
	{
		this.tests.add(test);
	}

	@Override
	public String toString() 
	{	
		return "Tests [tests=" + Arrays.toString(tests.toArray()) + "]";
	}
	
}
