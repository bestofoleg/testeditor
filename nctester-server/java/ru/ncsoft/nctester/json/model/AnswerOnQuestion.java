package ru.ncsoft.nctester.json.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.ncsoft.nctester.entity.QuestionTypeEntity;

@SuppressWarnings("serial")
public class AnswerOnQuestion implements Serializable
{
	
	private QuestionTypeEntity answerType;
	
	private List <String> answersMessages;
	
	public AnswerOnQuestion()
	{
		this.answersMessages = new ArrayList <> ();
	}
	
	public AnswerOnQuestion(List <String> answersMessages, QuestionTypeEntity answerType)
	{
		this.answersMessages = answersMessages;
		this.answerType = answerType;
	}

	public List<String> getAnswersMessages() 
	{
		return answersMessages;
	}

	public void setAnswersMessages(List<String> answersMessages) 
	{
		this.answersMessages = answersMessages;
	}
	
	public QuestionTypeEntity getAnswerType() 
	{
		return answerType;
	}

	public void setAnswerType(QuestionTypeEntity answerType) 
	{
		this.answerType = answerType;
	}

	@Override
	public String toString() 
	{
		return "Answer [answersMessages=" + Arrays.toString(this.answersMessages.toArray()) + "]";
	}
	
}
