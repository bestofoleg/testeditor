package ru.ncsoft.nctester.logic;

import java.io.IOException;
import java.io.StringWriter;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gwt.core.shared.GWT;

import ru.ncsoft.nctester.json.model.JSONString;

public class JsonConvertor 
{
	private JsonConvertor()
	{}
	
	public static JSONString entityToJSONString(Object entity)
	{
		ObjectMapper mapper = new ObjectMapper();
		StringWriter sw = new StringWriter();
		JSONString jstring = new JSONString();
		try {
			mapper.writeValue(sw, entity);
			jstring.setJstring(sw.toString());
		} catch (JsonGenerationException e) {
			GWT.log(e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			GWT.log(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			GWT.log(e.getMessage());
			e.printStackTrace();
		}finally {
			try {
				sw.close();
			} catch (IOException e) {
				GWT.log(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return jstring;
	}
}
