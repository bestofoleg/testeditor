package ru.ncsoft.nctester.logic;

import java.util.List;
import java.util.stream.Collectors;

import ru.ncsoft.nctester.entity.QuestionTypeEntity;
import ru.ncsoft.nctester.entity.TestEntity;
import ru.ncsoft.nctester.json.model.AnswersOnQuestions;
import ru.ncsoft.nctester.json.model.Assessment;

public class AssessmentSystem 
{
	
	private AssessmentSystem()
	{}
	
	public static Assessment getAssessment(AnswersOnQuestions answers, TestEntity testEntity)
	{
		
		Assessment result = new Assessment();
		
		int rightAnswersCount = 0;
		final int questionsCount = testEntity.getQuestion().size();
		
		for(int answerIndex = 0;answerIndex < questionsCount;answerIndex ++)
		{
			QuestionTypeEntity questionType = answers.getAnswersList().get(answerIndex).getAnswerType();
			if(questionType.equals(QuestionTypeEntity.TEXT))
			{
				String messageAnswer = answers.getAnswersList().get(answerIndex).getAnswersMessages().get(0);
				String rightAnswer = testEntity.getQuestion().get(answerIndex).getRightAnswerText();
				if(messageAnswer.equals(rightAnswer)) 
				{rightAnswersCount ++;}
			}
			else
			{
				List <String> answersChoices = answers.getAnswersList().get(answerIndex)
						.getAnswersMessages();
				List <String> rightChoices = testEntity.getQuestion().get(answerIndex)
						.getChoice().stream().filter(p -> p.getIsRightAnswer()).map(c -> c.getTitle())
						.collect(Collectors.toList());
				
				System.out.println(answersChoices);
				System.out.println(rightChoices);
				
				if(answersChoices.equals(rightChoices))
				{rightAnswersCount ++;}
				
			}
		}
		
		result.setRightAnswersQuantity(rightAnswersCount);
		result.setAnswersQuantity(questionsCount);
		
		return result;
	}
	
}
