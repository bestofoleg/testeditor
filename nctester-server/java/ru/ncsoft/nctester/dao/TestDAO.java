package ru.ncsoft.nctester.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import ru.ncsoft.nctester.entity.TestEntity;

@Repository
public class TestDAO implements IDAO<TestEntity>
{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public TestDAO()
	{}
	
	@Override
	public void save(TestEntity testEntity)
	{
        this.entityManager.merge(testEntity);   
	}
	
	@Override
	public List <TestEntity> getAll()
	{		
		return this.entityManager.createQuery("SELECT t FROM TestEntity t").getResultList();
	}
	
	@Override
	public TestEntity getById(Long id)
	{		
		return (TestEntity) entityManager.createQuery("SELECT t FROM TestEntity t WHERE t.id= :id")
                .setParameter("id",id).getResultList().get(0);
	}
	
	@Override
	public List <TestEntity> getByQueryString(String str)
	{		
		return entityManager.createQuery("SELECT t FROM TestEntity t WHERE t.title= :title")
                .setParameter("title",str).getResultList();
	}
	
}
