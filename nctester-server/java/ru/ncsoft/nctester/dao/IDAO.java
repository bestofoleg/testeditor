package ru.ncsoft.nctester.dao;

import java.util.List;

import ru.ncsoft.nctester.entity.TestEntity;

public interface IDAO <T>
{
	
	public void save(T entity);
	
	public List <T> getAll();
	
	public List <T> getByQueryString(String str);

	TestEntity getById(Long id);
	
}
